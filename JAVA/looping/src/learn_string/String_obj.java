package learn_string;

public class String_obj {
	String name;
	int age;
	public String_obj(String name,int age) {
		this.name=name;
		this.age=age;
	}
	public String toString() {
		return this.name+" "+this.age;
	}
	public static void main(String[] args) {
		String name=new String("monisha");
		String_obj so=new String_obj("hara",20);
		System.out.println(name);
		System.out.println(so);
		//byte no=10+5;
		byte no1=10;
		byte no2=5;
		//byte no=no1+no2;
		//short a=5+5;
		short a=5;
		short b=5;
		System.out.println(a+b);
}
}