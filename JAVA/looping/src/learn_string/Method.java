package learn_string;

public class Method {
public static void main(String[] args) {
	String name="moni";
	String name2="moni";
	System.out.println(name.charAt(0));
	System.out.println(name.equals(name2));
	System.out.println(name.equalsIgnoreCase(name2));
	System.out.println(name.hashCode());
	System.out.println(name2.hashCode());
	System.out.println(name.startsWith("m"));
	System.out.println(name.endsWith("o"));
	String name3="hara";
	System.out.println(name3.isEmpty());
	System.out.println(name3.indexOf('m'));
	System.out.println(name3.lastIndexOf('i'));
	String name4="papu";
	String name5=" monisha ";
	System.out.println(name4.substring(2));
	System.out.println(name4.substring(2,3));
	char[]ar=name5.toCharArray();
	for(char ch:ar) {
	System.out.println(ch+" ");}
	for(int i=ar.length-1;i>=0;i--) {
	System.out.println(ar[i]+" ");}
	System.out.println(name4.toLowerCase());
	System.out.println(name4.toUpperCase());
	String name6="14/12/2023";
	System.out.println( name5.trim());
	System.out.println(name5.strip());
	System.out.println(name5.stripLeading());
	System.out.println(name5.stripTrailing());
	String[]str=name6.split("3");
	for(String string:str) {
	
	System.out.println(string);} 
	String date=String.join("/","14","12","2023","hara");
	System.out.println(date);
}
}