package looping;

public class Row_loop {
public static void main(String[] args) {
	int row=5;
	while(row>=1) {
		int col=5;
		while(col>=1) {
			System.out.print(col);
			col=col-1;
		}
		row=row-1;
		System.out.println();
	}
}
}
