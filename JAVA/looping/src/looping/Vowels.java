package looping;

public class Vowels {

	   public static void main(String[] args) {
	        String name = "Hello Qantler";
	        String outputStr = RpVowel(name);

	        System.out.println("name:  " + name);
	        System.out.println("Output: " + outputStr);
	    }

	    public static String RpVowel(String inputString) {
	       
	        String[] vowels = {"a", "e", "i", "o", "u"};
	        String[] replace = {"@", "$", "&", "%", "#"};

	     
	        char[] charArray = inputString.toCharArray();

	        for (int i = 0; i < charArray.length; i++) {
	      
	            for (int j = 0; j < vowels.length; j++) {
	                if (Character.toLowerCase(charArray[i]) == vowels[j].charAt(0)) {
	                    charArray[i] = replace[j].charAt(0);
	                    break; 
	                }
	            }
	        }

	        return new String(charArray);
	    }

}


