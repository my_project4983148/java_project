package looping;

public class Column_loop {
	public static void main(String[] args) {
		int row=0;
		while (row<=5) {
			int col=1;
			while(col<=5) {
				System.out.print(row);
				col=col+1;
			}
			row=row+1;
			System.out.println();
		}
	}

}
