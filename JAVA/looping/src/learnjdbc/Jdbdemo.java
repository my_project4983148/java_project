package learnjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Jdbdemo {

	public static void main(String[] args) throws SQLException {
		Jdbdemo jd=new Jdbdemo();
		jd.select();
		jd.create();
		jd.update();
// when run more times then same value repeated and set.so command before methods.
	}

	private void update() throws SQLException {
		String url ="jdbc:postgresql://localhost:5432/learnjdbc";
		String userName="postgres";
		String password="moni";
		String query="update employee set salary =? where id=?";
		Connection con =DriverManager.getConnection(url,userName,password);
		PreparedStatement pst = con.prepareStatement(query)	;	
		pst.setInt(1, 90000);		
		pst.setInt(2,2);
		 int rows= pst.executeUpdate();
		System.out.println(rows);
		con.close();
				
	}

	private void create() throws SQLException {
		String url ="jdbc:postgresql://localhost:5432/learnjdbc";
		String userName="postgres";
		String password="moni";
		String query="insert into employee values (?,?,?)";
		Connection con =DriverManager.getConnection(url,userName,password);
		PreparedStatement pst = con.prepareStatement(query)	;	
		pst.setInt(1, 2);
		pst.setString(2,"moni");
		pst.setInt(3,80000);
		 int rows= pst.executeUpdate();
		System.out.println(rows);
		con.close();
		
	}

	private void select() throws SQLException {
		String url ="jdbc:postgresql://localhost:5432/learnjdbc";
		String userName="postgres";
		String password="moni";
		String query="select * from employee";
		Connection con =DriverManager.getConnection(url,userName,password);
		Statement st =con.createStatement();
	      ResultSet rs= st.executeQuery(query);
	      
	      while(rs.next())
	      {
	    	  System.out.println(rs.getInt(1));
	    	  System.out.println(rs.getString(2));
	    	  System.out.println(rs.getInt(3));
	      }
		
		con.close();
	}

}
