package java8features;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Predicatedemo {
	public static void main(String[] args) {
		
	//	Predicate<Integer> p= (marks)-> (marks>=35);
	//	System.out.println(p.test(33));
	                   //  anonymous function these are  using wrapper class and lamda expression .its make a easy readability type.all class use these checking condition for easily.eg:String,integer,array etc..
	
int	[]	ar= {10,60,50,68,14};
Predicate<Integer> p=(no)-> (no%2==0);
Predicate<Integer> p2=(no)-> (no>30);


Predicatedemo pd= new  Predicatedemo();
//pd.check(p2.and(p),ar);
 //pd.check(p2.or(p),ar);
//pd.check(p.negate(),ar);

Function <String,Integer> f=(str)->str.length();
System.out.println(f.apply("moni"));
  // for (int i=0;i<ar.length;i++) {
//	System.out.println(p.test(ar[i]));

ArrayList<Integer>al=new ArrayList<Integer>();
al.add(10);
al.add(20);
System.out.println(al);
al.forEach(num-> System.out.println(num));
Consumer<Integer>c= (n)->System.out.println(n);
c.accept(30);




}
//	public void apply (String str) {
//		return str.length();
//	}

		
		
	
	private void check (Predicate<Integer> p,int[]ar) {
		 for (int i=0;i<ar.length;i++) {
				System.out.println(p.test(ar[i]));
		 }
		
//		if (marks>=35) {
//			return true;
//		}
//		else
//			return false;
		
	//}

	}
}
