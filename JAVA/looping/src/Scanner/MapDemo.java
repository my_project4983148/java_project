package Scanner;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {

	public static void main(String[] args) {
		LinkedHashMap hm=new LinkedHashMap();
		hm.put("dosai",45);
		hm.put("poori",30);
		hm.put("chapati",40);
		hm.put("biriyani",130);
		hm.put("egg rice",35);
		hm.put("ramba rice",55);
		System.out.println(hm);
		System.out.println(hm.get("biriyani"));
		System.out.println(hm.containsKey("ramba rice"));
		System.out.println(hm.containsValue(45));
		System.out.println(hm.remove("poori"));
		System.out.println(hm);
		System.out.println(hm.replace("ramba rice",55,70));
		System.out.println(hm);
		System.out.println(hm.entrySet());
		System.out.println(hm.keySet());
		System.out.println(hm.values());
	 Set s=hm.entrySet();
	 Iterator i=s.iterator();
	 while(i.hasNext()) {
//		 System.out.println(i.next());
		 Map.Entry me=(Map.Entry)i.next();
		 System.out.println(me.getKey()+"cost"+me.getValue());
		 me.setValue((int)me.getValue()+5);	 }
	 System.out.println(hm);
	}

}




