package ass;

import java.util.ArrayList;

public class Streams {
public static void main(String[] args) {
	Employees emp = new Employees(1,"hara",2);
	Employees emp2 = new Employees(2,"kalai",5);
	Employees emp3 = new Employees(3,"bharathi",7);
	ArrayList <Employees> al= new ArrayList<Employees>();
	al.add(emp);
	al.add(emp2);
	al.add(emp3);
	al.stream()
	.map(names -> names.getName())
	.forEach(System.out::println);
	
	
	al.stream()
    .filter(names -> names.getExp()>3)
    .map(names -> names.getName())
    .forEach(System.out::println);
	
	
	
}
}
