const form=document.querySelector("#form") 
const username=document.querySelector("#username")
const email=document.querySelector("#email")
const password=document.querySelector("#password")
const cpassword=document.querySelector("#cpassword")

form.addEventListener('submit',(Object)=>{
    if (validateinputs()==false)
    Object.preventDefault();
})
function validateInputs(){
    const username_value= username.value.trim()
    const email_value= email.value.trim()
    const password_value= password.value.trim()
    const cpassword_value= cpassword.value.ttrim()
    let success=true
 
 
 if(username_value==="")
   {
    success=false
    setError(username,'User Name Required')
   }
   else{
    setSuccess(username)
   }

   if(email_value==="")
   {
    success=false
    setError(email,'Email Id Required')
   }
   else if(validateEmail(email_value)==null )
   {
    success=false
    setError(email,'Invalid Email Id')
   }
   else{
    setSuccess(email)
   }
   if (password_value==="")
   {
    success=false
    setError(password,'password Required')
   }
   else if(password_value.length<8)
   {
    success=false
    setError(password,'password must have atleast 8 characters long')
   }
   else{
    setSuccess(password)
   }
   if (cpassword_value==="")
   {
    success=false
    setError(password,'cpassword Required')
   }
   else if(password_value!==cpassword_value)
   {
    success=false
    setError(cpassword,'password does not match')
   }
   else{
    setSuccess(cpassword)
   } 
   console.log(validateEmail())
   return success  
}

function setError(element,message)
{
    let input_group= element.parentElement
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=message
    input_group.classList.add('error')
    input_group.classList.remove('success')
}

function setSuccess(element)
{
    let input_group= element.parentElement
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=""
    input_group.classList.add('success')
    input_group.classList.remove('error')
}

function validateEmail(ev) {
    return String(ev).toLowerCase().match(
        /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
      );
  };
